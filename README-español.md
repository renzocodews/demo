
# README
# Aplicacion Web Calculadora

La aplicacion es un prototipo de calculadora web se basa en comandos basicos de una calculadora como sumar, restar, multiplciar y dividar, es un programa desarrollado en JAVA, y despliegue un WAR, el cual se verifica las fases de Maven.


### Herramientas

* Git
* Jenkins
* Jacoco
* J Unit 4
* Sonarqube
* JFrog Artifactory
* Jboss7.2

[DiguitalOcena]
(https://bitbucket.org/renzocodews/demo/src/master/images/img1.png)

## Comenzando 🚀

Cuenta con servicios para las oepracioens basicas de una calculadora y ademas con un contralador para el paso de mensajes

### Pre-requisitos 📋

* Maven
* Cuenta Bitbucket
* Cuenta Diguital Ocean

## Ejecutando las pruebas ⚙️

Pruebas unitarias con J Unit 4 y Sonarqube y Jacoco para medir la cobertura.

[Sonarqube]
(https://bitbucket.org/renzocodews/demo/src/master/images/img2.png)

## Despliegue 📦
Se lo realizo con una maquina virtual creado en Diguital Ocean con el sistema opertivo Centos el cual serviria como servidor ya que tendria instala Jboss 7
[JBoss7.2]
(https://bitbucket.org/renzocodews/demo/src/master/images/img4.png)

## Versionado 📌

Se lo realizo con Jfrog Artefactory para el versionamiento del artefcto tipo war creado en el desarrollo del al aplicacion.
[JFrogArtifactory]
(https://bitbucket.org/renzocodews/demo/src/master/images/img3.png)

## Autores ✒️

* **Renzo Jaramillo** - *Trabajo Inicial* - [renzocodews](https://bitbucket.org/renzocodews)


## Licencia 📄

Este proyecto está bajo la Licencia - mira el archivo [LICENSE.md](LICENSE.md) para detalles
