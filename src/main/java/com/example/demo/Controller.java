package com.example.demo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/calculadora")
public class Controller {
	
	@GetMapping(path="add/{num1}/{num2}")
	public String add(@PathVariable("num1") Integer num1, @PathVariable("num2") Integer num2) {
		return String.valueOf(num1+num2);
			
	}
	
	@GetMapping(path="/substract/{num1}/{num2}")
	public String substract(@PathVariable("num1") Integer num1,@PathVariable("num2") Integer num2) {
		return String.valueOf(num1-num2);
		//return "Opcion Restar: numero 1= "+(Integer.parseInt(num1)-Integer.parseInt(num2));	
	}
	
	@GetMapping(path="/multiply/{num1}/{num2}")
	public String multiply(@PathVariable("num1") Integer num1,@PathVariable("num2") Integer num2) {
		return String.valueOf(num1*num2);
		//return "Opcion Restar: numero 1= "+(Integer.parseInt(num1)-Integer.parseInt(num2));	
	}
	@GetMapping(path="/divide/{num1}/{num2}")
	public String divide(@PathVariable("num1") Integer num1,@PathVariable("num2") Integer num2) {
		if (num2.equals(0))
				{
					return "Segundo operando es cero";
				}
				return String.valueOf(num1/num2);
		}	
}
