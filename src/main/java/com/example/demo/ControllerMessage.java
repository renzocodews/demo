package com.example.demo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/message")
public class ControllerMessage {

	
	
	@GetMapping(path="/hello")
	public String sayHello() {
		return "Hello";	
	}
	
	@GetMapping(path="/bye")
	public String sayBye() {
		return "Bye";	
	}
	
	@GetMapping(path="/{friends}")
	public String sayFriends(@PathVariable String friends) {
		
		if(friends.equals("yes")) {
			
			System.out.println("You said  \"yes\"");
		}else {
			System.out.println("You said "+ friends);
		}
		return friends;	
	}
	
	
}
