package com.example.demo;



import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class ControllerTest {

	
	
	@Test
	void testAdd() throws Exception {
		Controller controlador = new Controller();
		//int resutladoEsperadoSuma = 25;
		assertEquals(controlador.add(10,902),"912");
		
	}
	@Test
	void testAdd1() throws Exception {
		Controller controlador = new Controller();
		//int resutladoEsperadoSuma = 25;
		assertEquals(controlador.add(6,54),"60");
		
	}

		
	@Test
	void testSubstract() {
		Controller controlador = new Controller();
		//int resutladoEsperadoSubstract = 8;
		assertEquals(controlador.substract(10,2),"8");
	}

	@Test
	void testMultiply() {
		Controller controlador = new Controller();
		//int resutladoEsperadoMulti = 25;
		assertEquals(controlador.multiply(5,5),"25");
	}
	
	@Test
	void testDivide() {
		Controller controlador = new Controller();
		//int resutladoEsperadoDivide= 5;
		assertEquals(controlador.divide(15,3),"5");
	}
	
}
