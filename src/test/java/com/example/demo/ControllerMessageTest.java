package com.example.demo;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class ControllerMessageTest {
	@Test
	void testSayHello() {
		ControllerMessage controlador = new ControllerMessage();
		assertEquals(controlador.sayHello(),"Hello");
	}
	
	@Test
	void testSayBye(){
		ControllerMessage controlador = new ControllerMessage();
		assertEquals(controlador.sayBye(),"Bye");
	}
	@Test
	void testSayFriends() {
		ControllerMessage controlador = new ControllerMessage();
		assertEquals(controlador.sayFriends("brother"),"brother");
	}

}
